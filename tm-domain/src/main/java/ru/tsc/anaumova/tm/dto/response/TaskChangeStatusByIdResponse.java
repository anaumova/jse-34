package ru.tsc.anaumova.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.model.Task;

@NoArgsConstructor
public final class TaskChangeStatusByIdResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIdResponse(@Nullable final Task task) {
        super(task);
    }

}