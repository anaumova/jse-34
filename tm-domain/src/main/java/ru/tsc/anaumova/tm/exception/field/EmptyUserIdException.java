package ru.tsc.anaumova.tm.exception.field;

import ru.tsc.anaumova.tm.exception.AbstractException;

public final class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error! User id is empty...");
    }

}