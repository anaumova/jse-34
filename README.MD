# TASK MANAGER

## DEVELOPER INFO

* **NAME:** Anastasia Naumova
* **E-MAIL:** anaumova@t1-consulting.ru
* **E-MAIL:** anaumova@tsconsulting.com

## SOFTWARE

* **OS:** Windows 10 (20H2)
* **JDK:** 14.0.2
* **IDE:** IntelliJ IDEA

## HARDWARE

* **CPU:** AMD Ryzen 7
* **RAM:** 16Gb
* **SSD:** 476Gb

## APPLICATION BUILD

```
mvn clean install
```

## APPLICATION RUN

```
java -jar ./task-manager.jar
```
