package ru.tsc.anaumova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.dto.request.ProjectRemoveByIdRequest;
import ru.tsc.anaumova.tm.dto.response.ProjectRemoveByIdResponse;
import ru.tsc.anaumova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken(), id);
        @NotNull ProjectRemoveByIdResponse response = getProjectEndpoint().removeProjectById(request);
        @Nullable final Project project = response.getProject();
        if (project == null) throw new ProjectNotFoundException();
    }

}