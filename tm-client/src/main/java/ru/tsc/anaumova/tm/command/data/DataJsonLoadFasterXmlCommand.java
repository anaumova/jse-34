package ru.tsc.anaumova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.dto.request.DataJsonLoadFasterXmlRequest;
import ru.tsc.anaumova.tm.enumerated.Role;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Load data from json file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        getDomainEndpoint().loadDataJsonFasterXml(new DataJsonLoadFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}