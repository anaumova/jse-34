package ru.tsc.anaumova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-change-status-by-id";

    @NotNull
    public static final String DESCRIPTION = "Change task status by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken(), id, status);
        getTaskEndpoint().changeTaskStatusById(request);
    }

}