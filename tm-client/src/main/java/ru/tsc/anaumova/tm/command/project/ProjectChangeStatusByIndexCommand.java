package ru.tsc.anaumova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-change-status-by-index";

    @NotNull
    public static final String DESCRIPTION = "Change project status by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(getToken(), index, status);
        getProjectEndpoint().changeProjectStatusByIndex(request);
    }

}