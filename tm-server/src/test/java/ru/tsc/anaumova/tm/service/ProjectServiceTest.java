package ru.tsc.anaumova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.anaumova.tm.api.repository.IProjectRepository;
import ru.tsc.anaumova.tm.api.service.IProjectService;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.exception.entity.ModelNotFoundException;
import ru.tsc.anaumova.tm.exception.field.*;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.repository.ProjectRepository;
import ru.tsc.anaumova.tm.util.DateUtil;

import java.util.List;
import java.util.UUID;

public class ProjectServiceTest {

    @NotNull
    final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static long INITIAL_SIZE;

    @Before
    public void init() {
        projectRepository.create(USER_ID_1, "test-1");
        projectRepository.create(USER_ID_1, "test-2");
        projectRepository.create(USER_ID_2, "test-3");
        INITIAL_SIZE = projectRepository.getSize();
    }

    @Test
    public void create() {
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.create("", "test"));
        Assert.assertThrows(EmptyNameException.class, () -> projectService.create(USER_ID_1, ""));
        projectService.create(USER_ID_1, "test");
        Assert.assertEquals(INITIAL_SIZE + 1, projectService.getSize());
    }

    @Test
    public void createWithDescription() {
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.create("", "test", "test"));
        Assert.assertThrows(EmptyNameException.class, () -> projectService.create(USER_ID_1, "", "test"));
        Assert.assertThrows(EmptyDescriptionException.class, () -> projectService.create(USER_ID_1, "test", ""));
        projectService.create(USER_ID_1, "test", "test");
        Assert.assertEquals(INITIAL_SIZE + 1, projectService.getSize());
    }

    @Test
    public void createWithDescriptionAndDate() {
        @NotNull final Project project = projectService.create(
                USER_ID_1,
                "test",
                "test",
                DateUtil.toDate("10.10.2021"),
                DateUtil.toDate("11.11.2021")
        );
        Assert.assertEquals(INITIAL_SIZE + 1, projectService.getSize());
        Assert.assertNotNull(project.getDateBegin());
        Assert.assertNotNull(project.getDateEnd());
    }

    @Test
    public void clear() {
        projectService.clear();
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projectsAll = projectService.findAll();
        Assert.assertEquals(INITIAL_SIZE, projectsAll.size());
        @NotNull final List<Project> projectsOwnedUser1 = projectService.findAll(USER_ID_1);
        Assert.assertEquals(2, projectsOwnedUser1.size());
        @NotNull final List<Project> projectsOwnedUser3 = projectService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(0, projectsOwnedUser3.size());
    }

    @Test
    public void updateById() {
        @NotNull final Project project = projectService.findOneByIndex(0);
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectService.updateById("", projectId, "test", "test"));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectService.updateById(USER_ID_1, "", "test", "test"));
        Assert.assertThrows(EmptyNameException.class,
                () -> projectService.updateById(USER_ID_1, projectId, "", "test"));
        Assert.assertThrows(ModelNotFoundException.class,
                () -> projectService.updateById(USER_ID_1, "not_project_id", "test", "test"));
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        projectService.updateById(USER_ID_1, projectId, newName, newDescription);
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());
    }

    @Test
    public void updateByIndex() {
        @NotNull final Integer projectIndex = 0;
        @NotNull final Project project = projectService.findOneByIndex(projectIndex);
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectService.updateByIndex("", projectIndex, "test", "test"));
        Assert.assertThrows(EmptyNameException.class,
                () -> projectService.updateByIndex(USER_ID_1, projectIndex, "", "test"));
        Assert.assertThrows(IncorrectIndexException.class,
                () -> projectService.updateByIndex(USER_ID_1, -1, "test", "test"));
        Assert.assertThrows(IncorrectIndexException.class,
                () -> projectService.updateByIndex(USER_ID_1, 10000, "test", "test"));
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        projectService.updateByIndex(USER_ID_1, projectIndex, newName, newDescription);
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final Project project = projectService.findOneByIndex(0);
        @NotNull final String projectId = project.getId();
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectService.changeProjectStatusById("", projectId, newStatus));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectService.changeProjectStatusById(USER_ID_1, "", newStatus));
        Assert.assertThrows(ModelNotFoundException.class,
                () -> projectService.changeProjectStatusById(USER_ID_1, "not_project_id", newStatus));
        projectService.changeProjectStatusById(USER_ID_1, projectId, newStatus);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(newStatus, project.getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() {
        @NotNull final Integer projectIndex = 0;
        @NotNull final Project project = projectService.findOneByIndex(projectIndex);
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectService.changeProjectStatusByIndex("", projectIndex, newStatus));
        Assert.assertThrows(IncorrectIndexException.class,
                () -> projectService.changeProjectStatusByIndex(USER_ID_1, -1, newStatus));
        Assert.assertThrows(IncorrectIndexException.class,
                () -> projectService.changeProjectStatusByIndex(USER_ID_1, 10000, newStatus));
        projectService.changeProjectStatusByIndex(USER_ID_1, projectIndex, newStatus);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(newStatus, project.getStatus());
    }

    @Test
    public void findOneById() {
        @NotNull final String projectName = "test find by id";
        @NotNull final Project project = projectService.create(USER_ID_1, projectName);
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(EmptyIdException.class, () -> projectService.findOneById(""));
        Assert.assertNotNull(projectService.findOneById(projectId));
        Assert.assertEquals(projectName, projectService.findOneById(projectId).getName());
        Assert.assertThrows(ModelNotFoundException.class,
                () -> projectService.findOneById(UUID.randomUUID().toString()));
        Assert.assertNotNull(projectService.findOneById(USER_ID_1, projectId));
        Assert.assertEquals(projectName, projectService.findOneById(USER_ID_1, projectId).getName());
        Assert.assertThrows(ModelNotFoundException.class,
                () -> projectService.findOneById(USER_ID_1, UUID.randomUUID().toString()));
    }

    @Test
    public void findOneByIndex() {
        @NotNull final String projectName = "test find by index";
        projectService.create(USER_ID_1, projectName);
        @NotNull final Integer projectIndex = 2;
        Assert.assertThrows(IncorrectIndexException.class,
                () -> projectService.findOneByIndex(USER_ID_1, -1));
        Assert.assertThrows(IncorrectIndexException.class,
                () -> projectService.findOneByIndex(USER_ID_1, 1000));
        Assert.assertNotNull(projectService.findOneByIndex(USER_ID_1, projectIndex));
        Assert.assertEquals(projectName, projectService.findOneByIndex(USER_ID_1, projectIndex).getName());
    }

    @Test
    public void existsById() {
        @NotNull final String projectName = "test exist by id";
        @NotNull final Project project = projectService.create(USER_ID_1, projectName);
        @NotNull final String projectId = project.getId();
        Assert.assertTrue(projectService.existsById(projectId));
        Assert.assertFalse(projectService.existsById(UUID.randomUUID().toString()));
    }

    @Test
    public void remove() {
        @NotNull final Project project = projectService.create(USER_ID_1, "test");
        @NotNull final String projectId = project.getId();
        projectService.remove(project);
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.findOneById(projectId));
        Assert.assertEquals(INITIAL_SIZE, projectService.getSize());
        projectService.add(project);
        projectService.remove(USER_ID_1, project);
        Assert.assertThrows(ModelNotFoundException.class,
                () -> projectService.findOneById(USER_ID_1, projectId));
        Assert.assertEquals(INITIAL_SIZE, projectService.getSize());
    }

    @Test
    public void removeById() {
        @NotNull final Project project = projectService.create(USER_ID_1, "test");
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(EmptyIdException.class, () -> projectService.removeById(""));
        projectService.removeById(projectId);
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.findOneById(projectId));
        Assert.assertEquals(INITIAL_SIZE, projectService.getSize());
        projectService.add(project);
        Assert.assertThrows(EmptyIdException.class, () -> projectService.removeById(USER_ID_1, ""));
        projectService.removeById(USER_ID_1, projectId);
        Assert.assertThrows(ModelNotFoundException.class,
                () -> projectService.findOneById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertEquals(INITIAL_SIZE, projectService.getSize());
    }

    @Test
    public void removeByIndex() {
        projectService.create(USER_ID_1, "test");
        @NotNull final Integer projectIndex = 2;
        Assert.assertThrows(IncorrectIndexException.class,
                () -> projectService.removeByIndex(USER_ID_1, -1));
        Assert.assertThrows(IncorrectIndexException.class,
                () -> projectService.removeByIndex(USER_ID_1, 1000));
        projectService.removeByIndex(USER_ID_1, projectIndex);
        Assert.assertEquals(INITIAL_SIZE, projectService.getSize());
    }

}